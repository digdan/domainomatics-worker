var fs = require('fs');
var debug = require('debug')('words');
var allWords = [];

module.exports = function (input,minLength) {
	var words;
	if (minLength == undefined) minLength = 4;
	if (allWords.length == 0) { //Load
		var words = fs.readFileSync("data/words.txt","utf8").split('\r\n').filter(function(word) {
			return word.length >= minLength;
		});
		allWords = words;
	} else {
		words = allWords;
	}	
	if (words === null) words = allWords;
	var output=[];
	words.forEach( function(word) {
		if (input.match(word)) output.push(word);
	});
	return output;
}
