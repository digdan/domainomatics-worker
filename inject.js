#!/bin/env node
var async = require("async");
var os = require("os");
var cluster = require("cluster");
var redis = require("redis");
var words = require('./words.js');
var progress = require('progress');
var inspector = require('../inspector/inspector.js'); //Domain inspector

var expires = ( 60 * 60 * 24 * 64 );
var maxRecords = 3000;
var workerLimit = ( os.cpus().length ); // Full Throttle
//var workerLimit = ( os.cpus().length / 2 ); // Half Throttle
var pending = 0; //Pending tasks on a worker
var dbBar;

// Redis server used by webserver
var remoteClient = redis.createClient({
	host:'216.189.156.111',
	port:16379,
	password:'d0mad0ma0100',
	db:'domainomatics'
});
remoteClient.on("error", function(err) {
	debug('Remote Client Error : '+err);
});

// Workers redis workertable
var localClient = redis.createClient({
	port:16379,
	db:'preQualify'
});
localClient.on("error", function( err) {
	debug('Local Client Error : '+err);
	process.exit();
});

if (cluster.isMaster) {
	var debug = require("debug")("inject Master");
	var workers = [];
	var objCount=0;
	//Create list
	debug('Building domain list');
	localClient.zrevrangebyscore([
		'shortList', '+inf', '-inf'
	], function(err, response) {
		var curRecord = 0;
		maxRecords = response.length;
		remoteClient.set('domainCount',parseInt(maxRecords));
		var dbBar = new progress('Processing Records [:bar] :percent :current/:total (:elapsed/:eta)',{
			complete: '=',
			incomplete: ' ',
			width:100,
			total:parseInt(maxRecords)
		});
		debug('Clearing remote lists');
		remoteClient.del(['domainScores','searchList'], function(err,results) {
			if (err) console.log(err);
			debug('Cleared previous scores and search lists');
			//Build Workers
			for(var i= 0; i < workerLimit;i++) {
				debug('Spawning worker '+i);
				workers[i] = cluster.fork({
					workerIndex:i,
					workerLoad:(maxRecords / workerLimit)
				});
				workers[i].on('message', function(docKey) {
					objCount++;
					dbBar.tick();
					if (objCount == maxRecords) {
						debug('Done.');
						process.exit();
					}
				});
			}
			debug('Master is delegating tasks');
			response.forEach( function( res ) {
				curRecord++;
				var targetIndex = (curRecord % workerLimit ); //Modulate for round-robin distribution
				workers[targetIndex].send(res);
			} );
			if (curRecord == maxRecords) {
				debug('Master has delegated all tasks.');
			}
		});
	});
} else if (cluster.isWorker) {
	var canDie = false;
	debug = require("debug")("injector "+process.env['workerIndex']);
	debug('Loading wordlists');
	words('.');
	process.on('message', function(docKey) {
		if (docKey == 'die') {
			canDie = true;
			if (pending == 0) {
				debug('All tasks completed.');
				process.exit();
			}
		} else {
			pending++;
			localClient.get(docKey, function(err,docJSON) {
				doc = JSON.parse(docJSON);				
				var docGetKey = "domain:"+doc['domain'];
				if (doc === undefined) {
					debug('missing document : '+docGetKey);
				} else {
					var largestLength = 0;
					var domainWords = words(doc['textSLD']);					
					doc['words'] = JSON.stringify( Object.assign({},domainWords) );
					domainWords.forEach( function (word ) {
						if ( word.length > largestLength ) largestLength = word.length;
					});
					doc['factorWords'] = (( largestLength / doc['textSLD'].length ) * 100);


					doc['expireTimestamp'] = ( parseInt( Date.parse( doc['expires'] ) ) / 1000); //When the domain expires

					doc['score'] = Math.round( 
						( doc['factorTLD'] * 5 ) +
						( doc['factorSLD'] * 3 ) +
						( doc['factorSLDVowel'] * 0.1 ) +
						( doc['factorSyllables'] * 4 ) +
						( doc['factorWords'] * 5 ) );

					if (doc['domain'] == undefined) return;

					remoteClient.hmset(docGetKey,doc); //Define document
					remoteClient.expire(docGetKey, expires ); // When the redis set expires
					remoteClient.zadd([ 'domainList', doc['expireTimestamp'], docGetKey ]);	// Add to sorted set indexed by expiration timestamp
					remoteClient.zadd([ 'domainScores', doc['score'], docGetKey ]); //Add to a sorted set indexed by score
					var buffer = '';
					for (var i = 0, len = doc['domain'].length; i < len; i++) {
						buffer = buffer + doc['domain'][i];
						if ((i+1) == len) buffer = buffer + "*";
						remoteClient.zadd([ 'searchList', 0, buffer ]); //Add to searchable list
					}

					pending--;
					process.send(docGetKey); //Inform master we are completed

					if (canDie && (pending == 0)) {
						debug('All pending tasks completed.');
						process.exit();
					}				
				}
			});
		}
	});
}
