#!/bin/env node
// Downloads pools data, and imports records into a MongoDB Que
var progress = require('progress');
var debug = require('debug')('preQualify');
var domainParser = require('domain-name-parser');
var csv = require('fast-csv');
var fs = require('fs');
var redis = require('redis');
var exec = require('child_process').exec;
var client = redis.createClient({
	port:16379,
	db:'preQualify'	
});
client.on("error", function (err) {
	debug('DB Error '+err);
});
client.on("end", function() {
	debug('DB closed');
});

function textAnalyze(domain) {
	var text = {};
        var d = domainParser(domain);
        text.tld = d.tld;
        text.sld = d.sld;
        text.hyphen = Boolean(/[\-]/.test(d.sld));
        text.numeric = Boolean(/[0-9]/.test(d.sld));
        word = d.sld.toLowerCase();
        if(word.length <= 3) text.syllables = 1;
        word = word.replace(/(?:[^laeiouy]es|ed|[^laeiouy]e)$/, '');
        word = word.replace(/^y/, '');
        try {
        	text.syllables = word.match(/[aeiouy]{1,2}/g).length;
        } catch(e) {
        	text.syllables = null;
        }
        text.domainLength = d.sld.length;

	return text;
}

function isVowel(c) {
    return ['a', 'e', 'i', 'o', 'u'].indexOf(c.toLowerCase()) !== -1
}

function factorLetters(text) {
	var score = 0;
	var word = text.sld;
	var i = word.length;
	while (i--) {
		var c = word[i];
		if (c == 'a') score += 1;
		if (c == 'b') score += 3;
		if (c == 'c') score += 3;
		if (c == 'd') score += 2;
		if (c == 'e') score += 1;
		if (c == 'f') score += 4;
		if (c == 'g') score += 2;
		if (c == 'h') score += 4;
		if (c == 'i') score += 1;
		if (c == 'j') score += 8;
		if (c == 'k') score += 5;
		if (c == 'l') score += 1;
		if (c == 'm') score += 3;
		if (c == 'n') score += 1;
		if (c == 'o') score += 1;
		if (c == 'p') score += 3;
		if (c == 'q') score += 10;
		if (c == 'r') score += 1;
		if (c == 's') score += 1;
		if (c == 't') score += 1;
		if (c == 'u') score += 1;
		if (c == 'v') score += 4;
		if (c == 'w') score += 4;
		if (c == 'x') score += 8;
		if (c == 'y') score += 4;
		if (c == 'z') score += 10;
		if (c == '-') score += 500;
		if (c == '0') score += 100;
		if (c == '1') score += 100;
		if (c == '2') score += 100;
		if (c == '3') score += 100;
		if (c == '4') score += 100;
		if (c == '5') score += 100;
		if (c == '6') score += 100;
		if (c == '7') score += 100;
		if (c == '8') score += 100;
		if (c == '9') score += 100;
	}
	return Math.round( (300 / score),2 );
}

function factorVowel(text) {
	var i = text.sld.length;
	var vowelCount = 0;
	while(i--) {
		if (isVowel(text.sld[i])) vowelCount++;	
	}
	return vowelCount;
}

function factorTLD(text) {
	var score = 0;
	if (text.tld == 'com') score = 1;
	if (text.tld == 'net') score = .8;
	if (text.tld == 'org') score = .8;
	if (text.tld == 'edu') score = .75;
	if (text.tld == 'gov') score = .75;
	if (text.tld == 'info') score = .5;

	if (score == 0) {
		score = (.3 * ( 3 / text.tld.length ));
	}

	return score * 100;
}

function factorSyllables(text) {
	var score =0;
	if (text.syllables) {
		if (text.syllables == 1) score = 0.8 
		if (text.syllables == 2) score = 1;
		if (text.syllables == 3) score = 0.8;
		if (text.syllables == 4) score = 0.7;
		if (text.syllables == 5) score = 0.5;
		if (text.syllables == 6) score = 0.4;
		if (text.syllables == 7) score = 0.3;
		if (text.syllables == 8) score = 0.2;
		if (text.syllables > 8) score = Math.round(( 8 / text.syllables ) * 0.2 );
	}

	return score * 100;
}

//Function to initiate streams
function parsePool(filename,callback) {
	var data = [];
	var dataLen = 0;
	var recordsDone=0;
	var readFinished=false;
	var procMap=[];
	var recordCount=0;

	exec('wc -l '+filename, function(err,results) {
		var resultsParts = results.split(' ');
		recordCount = resultsParts[0];
		debug('Found '+recordCount+' records.');
		var dbBar = new progress('Processing Records [:bar] :percent :current/:total (:elapsed/:eta)',{
			complete: '=',
			incomplete: ' ',
			width:100,
			total:parseInt(recordCount)
		});
		csv
		.fromPath( filename )
		.on("data", function(data) {
			var domain = data[0];
			if (domain === undefined) {
				return false;
			}
			var expires = new Date(Date.parse(data[1]));
			var text = textAnalyze(domain);
			var letterFactor = factorLetters(text); //Scale of 1
			var sylFactor = factorSyllables(text); //Scale of 1
			var tldFactor = factorTLD(text); //Scale of 1
			var vowelPerc = (factorVowel(text) / domain.length);
			var vowelBase = Math.abs( 0.5 - vowelPerc ) * 2;
			var vowelFactor = Math.round((1 - vowelBase ) / 0.01,2); //Closer to even count of vowels to constants brings closer to 100

			var masterFactor = (tldFactor + letterFactor + sylFactor); //Sortable factoring
	
			var obj = {
				domain:domain,
				expires:expires,
				score:masterFactor,
		
				textTLD:text['tld'],
				textSLD:text['sld'],
				textHyphen:text['hyphen'],
				textNumeric:text['numeric'],
				textSyllables:text['syllables'],
				
				factorTLD:tldFactor,
				factorSLD:letterFactor,
				factorSLDVowel:vowelFactor,
				factorSyllables:sylFactor
		
			};
			client.set('domain:'+domain, JSON.stringify(obj) );
			client.zadd('shortList',masterFactor,'domain:'+domain);
			recordsDone++;
			dbBar.tick();
			if (recordCount==recordsDone) {
				debug('All records processed.');
				process.exit();
			}
		})
	});
}

debug('Clearing previous data.');
client.flushdb();
debug('PreQualifying');
parsePool('data/PoolDeletingDomainsList.txt');
